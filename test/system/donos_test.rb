require "application_system_test_case"

class DonosTest < ApplicationSystemTestCase
  setup do
    @dono = donos(:one)
  end

  test "visiting the index" do
    visit donos_url
    assert_selector "h1", text: "Donos"
  end

  test "creating a Dono" do
    visit donos_url
    click_on "New Dono"

    click_on "Create Dono"

    assert_text "Dono was successfully created"
    click_on "Back"
  end

  test "updating a Dono" do
    visit donos_url
    click_on "Edit", match: :first

    click_on "Update Dono"

    assert_text "Dono was successfully updated"
    click_on "Back"
  end

  test "destroying a Dono" do
    visit donos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Dono was successfully destroyed"
  end
end
