require "application_system_test_case"

class EstacionamentosTest < ApplicationSystemTestCase
  setup do
    @estacionamento = estacionamentos(:one)
  end

  test "visiting the index" do
    visit estacionamentos_url
    assert_selector "h1", text: "Estacionamentos"
  end

  test "creating a Estacionamento" do
    visit estacionamentos_url
    click_on "New Estacionamento"

    fill_in "Gastos", with: @estacionamento.Gastos
    fill_in "Lucro Bruto", with: @estacionamento.Lucro_Bruto
    fill_in "Lucro Liquido", with: @estacionamento.Lucro_Liquido
    fill_in "Planos", with: @estacionamento.Planos
    fill_in "Vagas", with: @estacionamento.Vagas
    fill_in "Valor H", with: @estacionamento.Valor_h
    click_on "Create Estacionamento"

    assert_text "Estacionamento was successfully created"
    click_on "Back"
  end

  test "updating a Estacionamento" do
    visit estacionamentos_url
    click_on "Edit", match: :first

    fill_in "Gastos", with: @estacionamento.Gastos
    fill_in "Lucro Bruto", with: @estacionamento.Lucro_Bruto
    fill_in "Lucro Liquido", with: @estacionamento.Lucro_Liquido
    fill_in "Planos", with: @estacionamento.Planos
    fill_in "Vagas", with: @estacionamento.Vagas
    fill_in "Valor H", with: @estacionamento.Valor_h
    click_on "Update Estacionamento"

    assert_text "Estacionamento was successfully updated"
    click_on "Back"
  end

  test "destroying a Estacionamento" do
    visit estacionamentos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Estacionamento was successfully destroyed"
  end
end
