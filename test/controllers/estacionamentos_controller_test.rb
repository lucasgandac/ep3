require 'test_helper'

class EstacionamentosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @estacionamento = estacionamentos(:one)
  end

  test "should get index" do
    get estacionamentos_url
    assert_response :success
  end

  test "should get new" do
    get new_estacionamento_url
    assert_response :success
  end

  test "should create estacionamento" do
    assert_difference('Estacionamento.count') do
      post estacionamentos_url, params: { estacionamento: { Gastos: @estacionamento.Gastos, Lucro_Bruto: @estacionamento.Lucro_Bruto, Lucro_Liquido: @estacionamento.Lucro_Liquido, Planos: @estacionamento.Planos, Vagas: @estacionamento.Vagas, Valor_h: @estacionamento.Valor_h } }
    end

    assert_redirected_to estacionamento_url(Estacionamento.last)
  end

  test "should show estacionamento" do
    get estacionamento_url(@estacionamento)
    assert_response :success
  end

  test "should get edit" do
    get edit_estacionamento_url(@estacionamento)
    assert_response :success
  end

  test "should update estacionamento" do
    patch estacionamento_url(@estacionamento), params: { estacionamento: { Gastos: @estacionamento.Gastos, Lucro_Bruto: @estacionamento.Lucro_Bruto, Lucro_Liquido: @estacionamento.Lucro_Liquido, Planos: @estacionamento.Planos, Vagas: @estacionamento.Vagas, Valor_h: @estacionamento.Valor_h } }
    assert_redirected_to estacionamento_url(@estacionamento)
  end

  test "should destroy estacionamento" do
    assert_difference('Estacionamento.count', -1) do
      delete estacionamento_url(@estacionamento)
    end

    assert_redirected_to estacionamentos_url
  end
end
