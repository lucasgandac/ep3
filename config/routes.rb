Rails.application.routes.draw do
  resources :donos
  resources :estacionamentos
  resources :funcionarios
  resources :usuarios
  resources :veiculos
  resources :pessoas
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
