class EstacionamentosController < ApplicationController
  before_action :set_estacionamento, only: [:show, :edit, :update, :destroy]

  # GET /estacionamentos
  # GET /estacionamentos.json
  def index
    @estacionamentos = Estacionamento.all
  end

  # GET /estacionamentos/1
  # GET /estacionamentos/1.json
  def show
  end

  # GET /estacionamentos/new
  def new
    @estacionamento = Estacionamento.new
  end

  # GET /estacionamentos/1/edit
  def edit
  end

  # POST /estacionamentos
  # POST /estacionamentos.json
  def create
    @estacionamento = Estacionamento.new(estacionamento_params)

    respond_to do |format|
      if @estacionamento.save
        format.html { redirect_to @estacionamento, notice: 'Estacionamento was successfully created.' }
        format.json { render :show, status: :created, location: @estacionamento }
      else
        format.html { render :new }
        format.json { render json: @estacionamento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /estacionamentos/1
  # PATCH/PUT /estacionamentos/1.json
  def update
    respond_to do |format|
      if @estacionamento.update(estacionamento_params)
        format.html { redirect_to @estacionamento, notice: 'Estacionamento was successfully updated.' }
        format.json { render :show, status: :ok, location: @estacionamento }
      else
        format.html { render :edit }
        format.json { render json: @estacionamento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /estacionamentos/1
  # DELETE /estacionamentos/1.json
  def destroy
    @estacionamento.destroy
    respond_to do |format|
      format.html { redirect_to estacionamentos_url, notice: 'Estacionamento was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_estacionamento
      @estacionamento = Estacionamento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def estacionamento_params
      params.require(:estacionamento).permit(:Vagas, :Planos, :Valor_h, :Lucro_Bruto, :Lucro_Liquido, :Gastos)
    end
end
