json.extract! dono, :id, :created_at, :updated_at
json.url dono_url(dono, format: :json)
