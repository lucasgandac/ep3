class Usuario < ApplicationRecord
  has_many :veiculos, class_name: "veiculo", foreign_key: "reference_id"
  it { should belong_to(:pessoa) } 
end
