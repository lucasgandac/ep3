class Estacionamento < ApplicationRecord
    has_many :funcionarios, class_name: "funcionario", foreign_key: "reference_id"
    has_many :usuarios, class_name: "usuario", foreign_key: "reference_id"
    it { should belong_to(  :dono) } 
end
