class Dono < ApplicationRecord
    has_many :estacionamentos, class_name: "estacionamento", foreign_key: "reference_id"
    it { should belong_to(:pessoa) } 
end
