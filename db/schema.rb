# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_02_165126) do

  create_table "donos", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "estacionamentos", force: :cascade do |t|
    t.integer "Vagas"
    t.string "Planos"
    t.integer "Valor_h"
    t.integer "Lucro_Bruto"
    t.integer "Lucro_Liquido"
    t.integer "Gastos"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "funcionarios", force: :cascade do |t|
    t.string "Cargo"
    t.integer "Carros_Registrados"
    t.integer "Salario"
    t.integer "Carga_Horaria"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pessoas", force: :cascade do |t|
    t.string "Nome"
    t.integer "Idade"
    t.string "Cpf"
    t.string "Sexo"
    t.string "Endereco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "usuarios", force: :cascade do |t|
    t.integer "veiculo_id"
    t.string "Plano"
    t.integer "Horas"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["veiculo_id"], name: "index_usuarios_on_veiculo_id"
  end

  create_table "veiculos", force: :cascade do |t|
    t.string "Marca"
    t.string "Modelo"
    t.integer "Ano"
    t.string "Danos"
    t.string "Placa"
    t.string "Cor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
