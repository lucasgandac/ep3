class CreateUsuarios < ActiveRecord::Migration[5.2]
  def change
    create_table :usuarios do |t|
      t.references :veiculo, foreign_key: true
      t.string :Plano
      t.integer :Horas

      t.timestamps
    end
  end
end
