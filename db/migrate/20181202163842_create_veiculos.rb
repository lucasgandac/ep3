class CreateVeiculos < ActiveRecord::Migration[5.2]
  def change
    create_table :veiculos do |t|
      t.string :Marca
      t.string :Modelo
      t.integer :Ano
      t.string :Danos
      t.string :Placa
      t.string :Cor

      t.timestamps
    end
  end
end
