class CreateEstacionamentos < ActiveRecord::Migration[5.2]
  def change
    create_table :estacionamentos do |t|
      t.integer :Vagas
      t.string :Planos
      t.integer :Valor_h
      t.integer :Lucro_Bruto
      t.integer :Lucro_Liquido
      t.integer :Gastos

      t.timestamps
    end
  end
end
