class CreatePessoas < ActiveRecord::Migration[5.2]
  def change
    create_table :pessoas do |t|
      t.string :Nome
      t.integer :Idade
      t.string :Cpf
      t.string :Sexo
      t.string :Endereco

      t.timestamps
    end
  end
end
