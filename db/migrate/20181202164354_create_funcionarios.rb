class CreateFuncionarios < ActiveRecord::Migration[5.2]
  def change
    create_table :funcionarios do |t|
      t.string :Cargo
      t.integer :Carros_Registrados
      t.integer :Salario
      t.integer :Carga_Horaria

      t.timestamps
    end
  end
end
